# arquillian example

It is based on the [arquillian guide](http://arquillian.org/guides/) for exercise.


## Prerequisites

* installed JDK 8 and 7
* installed maven (tested on 3.5.3)
* installed GlassFish 3.1.2.2
* installed jboss-as-7.1.1.Final (compatible with JDK 7)

## Run

* mvn clean test -Parquillian-glassfish-embedded
* mvn clean test -Parquillian-glassfish-remote
* mvn clean test -Parquillian-jbossas-managed
* mvn clean test -Parquillian-jbossas-remote
* mvn clean test -Parquillian-weld-ee-embedded

The embedded or managed profiles can be run as is. The others requires the corresponding app server unpacked and started before running the integration tests.
